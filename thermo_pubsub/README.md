# README #

This repository contains the description and the support source code for the programming exercise IoT researcher/developer – University of Cambridge.

Please send any techncial questions or queries to rd530@cam.ac.uk, cc to js573@cam.ac.uk

### Prereqs ###

* MosquittoMQ
http://mosquitto.org/


### Deployment ###
* Building
```
#!sh
$ make

```

* Run mosquitto; By default it runs on port 1883.
```
#!sh
$ mosquitto

```
* Run a subscriber with the mosquitto broker addr and port; By default 127.0.0.1 and 1883 respectively.
```
#!sh
$ ./subscriber 127.0.0.1 1883

```

* Run one or more thermometers configured with the mosquitto broker addr and port;
```
#!sh
$ ./publisher 127.0.0.1 1883

```


