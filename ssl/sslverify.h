
#include <openssl/x509.h>


int verify(const char* certfile, const char* CAfile);


int spc_sign(unsigned char *msg, unsigned int mlen, 
             unsigned char *out, unsigned int *outlen, 
             RSA *r) ; // r is priv key
             
int spc_verify(unsigned char *msg, unsigned int mlen, 
               unsigned char *sig, unsigned int siglen, 
               RSA *r); // r is pub key

RSA* load_private_key(const char* path);
RSA* load_public_key(const char* path);
