

#include "sslverify.h"
#include <string.h>


int main(int argc, char **argv)
{
	// parse arguments
	if(argc<3)
	{
		printf("Usage: sslverify certif_file ca_file\n\tcertif_file eg /Users/admin/etc/ssl/mydomain.pem\n\tca_file eg /Users/admin/etc/ssl/demoCA/cacert.pem \n");
		exit(0);
	}
	
	/*if(verify(argv[1], argv[2]))
		printf("verification OK\n");
	else
		printf("verification failed\n");
	*/
	
	char* message = (char* ) malloc(10);
	strcpy(message,"holla");
	
	RSA* rsa_priv = load_private_key("/Users/admin/etc/ssl/mydomain.key");
	RSA* rsa_pub = load_public_key("/Users/admin/etc/ssl/mydomain.pubkey.pem");

	char* signature = (char*)malloc(251);
	unsigned int siglen = 250;
	spc_sign((unsigned char*)message, 4, (unsigned char*)signature, &siglen, rsa_priv);

	printf("signature: %s\n\n", signature);
	//int ret = RSA_verify(NID_sha1, hash, 20, signature, siglen, r);
	printf("verification: %d\n", spc_verify((unsigned char*)message, 4, (unsigned char*)signature, siglen, rsa_pub));
	
	return 0;
}
