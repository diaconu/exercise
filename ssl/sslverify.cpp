
#include <openssl/x509.h>

#include <openssl/lhash.h>
#include <openssl/buffer.h>
#include <openssl/pem.h>
#include <openssl/err.h>

#include <openssl/rsa.h>
#include <string.h>


#include "sslverify.h"

static X509 *load_cert(const char *file);
static int check(X509_STORE *ctx, const char *file);

int verify(const char* certfile, const char* CAfile)
{
    int ret=0;
    X509_STORE *cert_ctx=NULL;
    X509_LOOKUP *lookup=NULL;

    cert_ctx=X509_STORE_new();
    if (cert_ctx == NULL) goto end;

    OpenSSL_add_all_algorithms();

    lookup=X509_STORE_add_lookup(cert_ctx,X509_LOOKUP_file());
    if (lookup == NULL)
        goto end;

    if(!X509_LOOKUP_load_file(lookup,CAfile,X509_FILETYPE_PEM))
        goto end;

    lookup=X509_STORE_add_lookup(cert_ctx,X509_LOOKUP_hash_dir());
    if (lookup == NULL)
        goto end;

    X509_LOOKUP_add_dir(lookup,NULL,X509_FILETYPE_DEFAULT);

    ret = check(cert_ctx, certfile);
end:
    if (cert_ctx != NULL) X509_STORE_free(cert_ctx);

    return ret;
}

static X509 *load_cert(const char *file)
{
    X509 *x=NULL;
    BIO *cert;

    if ((cert=BIO_new(BIO_s_file())) == NULL)
        goto end;

    if (BIO_read_filename(cert,file) <= 0)
        goto end;

    x=PEM_read_bio_X509_AUX(cert,NULL, NULL, NULL);
end:
    if (cert != NULL) BIO_free(cert);
    return(x);
}

static int check(X509_STORE *ctx, const char *file)
{
    X509 *x=NULL;
    int i=0,ret=0;
    X509_STORE_CTX *csc;

    x = load_cert(file);
    if (x == NULL)
        goto end;

    csc = X509_STORE_CTX_new();
    if (csc == NULL)
        goto end;
    X509_STORE_set_flags(ctx, 0);
    if(!X509_STORE_CTX_init(csc,ctx,x,0))
        goto end;
    i=X509_verify_cert(csc);
    X509_STORE_CTX_free(csc);

    ret=0;
end:
    ret = (i > 0);
    if (x != NULL)
        X509_free(x);

    return(ret);
}

/*
 int RSA_sign(int type, const unsigned char *m, unsigned int m_len,
    unsigned char *sigret, unsigned int *siglen, RSA *rsa);

 int RSA_verify(int type, const unsigned char *m, unsigned int m_len,
    unsigned char *sigbuf, unsigned int siglen, RSA *rsa);
    */

char* text_load_from_file(const char* filename)
{
    FILE* _file = fopen(filename, "r");

    if (_file == NULL)
        return NULL;

    /* get size of file */
    fseek(_file, 0L, SEEK_END);
    int size = (int) ftell(_file);
    rewind(_file);

    /* read file */
    char* var;
    var = (char*) malloc((size_t) size + 1);
    memset(var, 0, (size_t) size + 1);

    fread (var, 1, (size_t) size, _file);
    fclose(_file);

    /* return text */
    return var;
}

int spc_sign(unsigned char *msg, unsigned int mlen, 
             unsigned char *out, unsigned int *outlen, 
             RSA *r) {
  unsigned char hash[20];
   
  if (!SHA1(msg, mlen, hash)) return 0;
  return RSA_sign(NID_sha1, hash, 20, out, outlen, r);
}

int spc_verify(unsigned char *msg, unsigned int mlen, 
               unsigned char *sig, unsigned int siglen, 
               RSA *r) {
  unsigned char hash[20];
  BN_CTX        *c;
  int           ret;
   
  if (!(c = BN_CTX_new(  ))) return 0;
  if (!SHA1(msg, mlen, hash) || !RSA_blinding_on(r, c)) {
    BN_CTX_free(c);
    return 0;
  }
  ret = RSA_verify(NID_sha1, hash, 20, sig, siglen, r);
  RSA_blinding_off(r);
  BN_CTX_free(c);
  return ret;
}

RSA* load_private_key(const char* path)
{
	char* priv_key = text_load_from_file(path);
	
	BIO* bo = BIO_new( BIO_s_mem() );
	BIO_write( bo, priv_key,strlen(priv_key));

	EVP_PKEY* pkey = 0;
	PEM_read_bio_PrivateKey( bo, &pkey, 0, 0 );

	BIO_free(bo);


	RSA* rsa = EVP_PKEY_get1_RSA( pkey );
	
	return rsa;
}

RSA* load_public_key(const char* path)
{
	char* pub_key = text_load_from_file(path);
	
	BIO* bo = BIO_new( BIO_s_mem() );
	BIO_write( bo, pub_key,strlen(pub_key));

	EVP_PKEY* pkey = 0;
	PEM_read_bio_PUBKEY( bo, &pkey, 0, 0 );

	BIO_free(bo);


	RSA* rsa = EVP_PKEY_get1_RSA( pkey );
	
	return rsa;
}

